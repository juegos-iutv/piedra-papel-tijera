import java.util.Scanner;
import java.util.Random;
	public class PiedraPapelTijera{
		public static void main(String[]arg){
			Scanner tec=new Scanner(System.in);
         
         int cpu=(int)(Math.random()*3)+1; //jugada aleatoria de CPU
         int usu;
         
         //comenzar juego
         System.out.println("\t~ Piedra Papel o Tijera ~");
         do{
            System.out.print("Elija su jugada [1-piedra] [2-papel] [3-tijera]: ");
            usu=tec.nextInt();
            
            if(usu<1 || usu>3){
               System.out.println("JUGADA NO VALIDA");
            }
         
         }while(usu<1 || usu>3);
         
         //jugadas
         System.out.print("\nUsuario: ");
         switch(usu){
            case 1:
               System.out.println("piedra");
            break;
            case 2:
               System.out.println("papel");
            break;
            case 3:
               System.out.println("tijera");
            break;
         }

         System.out.print("CPU: ");
         switch(cpu){
            case 1:
               System.out.println("piedra");
            break;
            case 2:
               System.out.println("papel");
            break;
            case 3:
               System.out.println("tijera");
            break;
         }
         
         //determinar ganador
         if(cpu==usu){
            System.out.println("> empate <");
         }
         
         if( (cpu==1 && usu==2) || (cpu==2 && usu==3) || (cpu==3 && usu==1)){
            System.out.println("> Usuario gana <");
         }
         if( (cpu==2 && usu==1) || (cpu==3 && usu==2) || (cpu==1 && usu==3)){
            System.out.println("> CPU gana <");
         }
         
      }
   }